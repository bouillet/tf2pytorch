import os
from genericpath import isfile, isdir
import shutil


import tensorflow as tf
import tf2onnx
from onnx_pytorch import code_gen
import numpy as np

class TF2Torch : 
    def __init__(self, model, model_type, target_size, dir_path=os.getcwd(), tmp_path="/tmp", model_path="", opset=13):

        self.model = model
        self.name = ""
        self.tmp_path = dir_path + tmp_path + "/"
        self.model_path = dir_path + model_path + "/"
        self.target_size = target_size
        if model_type == "tensorflow" : 
            self.model_type = "tf"
        elif model_type == "keras" :
            self.model = "keras"
        else : 
            try : 
                raise ValueError
            except ValueError :
                print("Only TensorFlow and Keras are supported. model_type must be either 'tensorflow' or 'keras'")
                raise
        ## add target_size validation => 3-element tuple with channel_first

        if np.shape(target_size) == (3,):
            #check channel_first
            if target_size[0]<target_size[1]/5: 
                self.target_size = target_size
            else :
                try:
                    raise ValueError
                except ValueError:
                    print("Channel-first input type is expected for the model ex : target_size=(3,224,224)")
                    raise

        else : 
            try:
                raise ValueError
            except ValueError:
                print("A 3-element tuple is expected for the target_size")
                raise


        ## add path_validation and folder creation

        if not isdir(self.tmp_path):
            os.mkdir(self.tmp_path)
        if not isdir(self.model_path): 
            os.mkdir(self.model_path)

        self.opset=opset

    def convert_tf2torch(self, keep_old=True, old_model = "model_old", old_path=False):
        self.convert_tf2onnx()
        self.convert_onnx2pytorch(keep_old, old_model, old_path)

    def convert_tf2onnx(self):
        #Check Model Name 
        if self.model.name : 
            name = self.model.name
        else : 
            name = "dummy_name"
        
        self.name = name

        spec = (tf.TensorSpec((None,self.target_size[0],self.target_size[1],self.target_size[2]), tf.float32, name='input'),)
        output_path = self.tmp_path + name + "_tf.onnx"
        model_proto = None

        if self.model_type == "keras" :
            model_proto, _ = tf2onnx.convert.from_keras(self.model, input_signature=spec, opset=self.opset, output_path=output_path)
        
        if self.model_type == "tf" :
            model_proto, _ = tf2onnx.convert.from_function(self.model, input_signature=spec, opset=self.opset, output_path=output_path)

        output_names = [n.name for n in model_proto.graph.output]


    def convert_onnx2pytorch(self, keep_old = True, old_model = "model_old", old_path=False):

        ### Check presence of model and delete or replace it 

        if keep_old :

            if not old_path:
                old_path = self.tmp_path

            if isfile(self.model_path + "model.py"):
                shutil.move(self.model_path + "model.py", old_path + old_model + ".py")
            if isdir(self.model_path + "variables"):
                shutil.move(self.model_path + "variables", old_path + old_model+ "variables")
        else : 
            if isfile(self.model_path + "model.py"):
                os.remove(self.model_path + "model.py")
            if isdir(self.model_path + "variables"):
                os.remove(self.model_path + "variables")

        ### Generate Pytorch code for translated model

        path_to_onnx = self.tmp_path + self.name + "_tf.onnx"
        code_gen.gen(path_to_onnx, self.model_path)
