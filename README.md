# tf2pytorch

Convert Tensorflow (not tested yet) and Keras models to Pytorch using ONNX Graph representation


# Install

Install Requirements
```bash
pip install -r requirements
```

Install Package
```bash
python setup.py install
```

# Run

* Import your own model (or pretrained Model). Here, pretrained Keras VGG16 model is used.

```python
import os
from tensorflow.keras.applications.vgg16 import VGG16

model = VGG16(weights="imagenet")
```

* Convert Keras model
```python
from tf2pt import TF2Torch
target_size = (3,224,224)

converter = TF2Torch(model, model_path="keras", target_size, model_path="")
converter.convert_tf2torch()
```

A new file "model.py" will be created in the model_path folder and contains the PyTorch class of your converted model, with weight and biasis initialized according to your previous Keras model. Weight and biases are stored in the "variables" folder

* Load your model.

```python
import torch
from model import Model
model = Model()
model = model.eval()
```


