from distutils.core import setup

setup(name='tf2torch',
      version='1.0',
      description='Model Converter from TensorFlow to Pytorch',
      author='Etienne Bouillot',
      author_email='etienne.bouillot@univ-grenoble-alpes.fr',
      url=''
     )